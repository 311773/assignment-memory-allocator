#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <unistd.h>

void *heap;
struct block_header *head;


#define HEAP_SIZE 10000
#define MALLOC_SIZE 1000

#define FIRST_BLOCK 1000
#define SECOND_BLOCK 2000
#define THIRD_BLOCK 5000
#define FOURTH_BLOCK 9000
#define MAXIMUM 50000

inline static int get_freed_blocks() {
    int freed = 0;
    struct block_header *next = head;
    while (next) {
        if (next->is_free) freed++;
        next = next->next;
    }
    return freed;
}

bool test1() {
    debug_heap(stdout, head);
    void* mal = _malloc(MALLOC_SIZE);
    debug_heap(stdout, head);
    if (mal == NULL || head->capacity.bytes != MALLOC_SIZE) return false;
    _free(mal);
    debug_heap(stdout, head);
    return true;
}

bool test2() {
    debug_heap(stdout, head);
    void *block1 = _malloc(FIRST_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    debug_heap(stdout, head);

    _free(block1);
    debug_heap(stdout, head);

    if (get_freed_blocks() != 2) return false;

    _free(block2);
    _free(block1);

    return true;
}

bool test3() {
    debug_heap(stdout, head);
    void *block1 = _malloc(FIRST_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    void *block3 = _malloc(THIRD_BLOCK);
    debug_heap(stdout, head);

    _free(block1);
    _free(block2);
    debug_heap(stdout, head);

    if (get_freed_blocks() != 3) return false;

    _free(block3);
    _free(block2);
    _free(block1);

    return true;
}

bool test4() {
    debug_heap(stdout, head);
    void *block4 = _malloc(FOURTH_BLOCK);
    void *block2 = _malloc(SECOND_BLOCK);
    debug_heap(stdout, head);

    int allocated = 0;
    struct block_header *next = head;
    while (next->next && ++allocated) next = next->next;

    if (allocated != 2) return false;

    _free(block2);
    _free(block4);

    return true;
}


bool test5() {
    head->capacity.bytes = HEAP_SIZE;
    debug_heap(stdout, head);

    struct block_header *next = head;
    while (next) next = next->next;

    uint8_t *mem = (uint8_t * )(getpagesize() * ((size_t) next / getpagesize() + ((size_t) next % getpagesize() > 1)));
    (void) mmap(mem, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);

    void *allocated = _malloc(MAXIMUM);

    if (mem == (uint8_t * )((uint8_t *) allocated - offsetof(struct block_header, contents))) return false;

    debug_heap(stdout, head);
    _free(allocated);

    return true;
}

typedef bool (*test)(void);

test tests[5] = {&test1, &test2, &test3, &test4, &test5};

inline static void print_separator() {
    for (int i = 0; i < 40; i++) printf("-");
    printf("\n");
}

int main() {
    heap = heap_init(HEAP_SIZE);
    head = (struct block_header *) heap;

    bool res = true;

    for (size_t i = 0; i < 5; i++) {
        print_separator();
        printf("TEST %zu\n", i + 1);
        bool res_i = tests[i]();
        if (res_i) {
            printf("\nTEST %zu PASSED\n", i + 1);
        } else
            printf("\nTEST %zu FAILED\n", i + 1);
        res &= res_i;
        print_separator();
    }
    
    if (res) {
	printf("ALL TESTS PASSED\n");
        return 0;
    }
    printf("TEST(S) FAILED\n");
    return 1;
}
